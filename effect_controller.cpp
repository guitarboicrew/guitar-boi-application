#include "effect_controller.h"

EffectController::EffectController(QObject *parent) : QObject(parent)
{

}

void EffectController::apply(float *out, jack_nframes_t len) {
    if (m_enabled) {
        apply_effect(out, len);
    }
}
