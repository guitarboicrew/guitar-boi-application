#include "jack_controller.h"
#include "jack.h"
#include <jack/jack.h>


JackController::JackController(QObject *parent) : QObject(parent)
{

}

void JackController::start() {
    if (is_running()) return;

    const char **ports;
    const char *client_name = "simple";
    const char *server_name = nullptr;
    jack_options_t options = JackNullOption;
    jack_status_t status;

    client = jack_client_open(client_name, options, &status, server_name);
    if (client == nullptr) {
        fprintf(stderr, "jack_client_open() failed, status = 0x%2.0x\n", status);
        if (status & JackServerFailed) {
            fprintf(stderr, "Unable to connect to JACK server\n");
        }
        sanitize();
        return;
    }
    if (status & JackServerStarted) {
        fprintf(stderr, "JACK server started\n");
    }
    if (status & JackNameNotUnique) {
        client_name = jack_get_client_name(client);
        fprintf(stderr, "unique name `%s' assigned\n", client_name);
    }

    /* tell the JACK server to call `process()' whenever
       there is work to be done.
    */

    process_args = new jack_process_args();
    process_args->used_effects = &m_effects;

    jack_set_process_callback(client, jack_process, process_args);

    /* tell the JACK server to call `jack_shutdown()' if
       it ever shuts down, either entirely, or if it
       just decides to stop calling us.
    */

    jack_on_shutdown(client, jack_shutdown, this);

    /* display the current sample rate.
     */

    printf("engine sample rate: %" PRIu32 "\n",
           jack_get_sample_rate(client));

    /* create two ports */

    input_port = jack_port_register(client, "input",
                                    JACK_DEFAULT_AUDIO_TYPE,
                                    JackPortIsInput, 0);
    output_port = jack_port_register(client, "output",
                                     JACK_DEFAULT_AUDIO_TYPE,
                                     JackPortIsOutput, 0);

    if ((input_port == nullptr) || (output_port == nullptr)) {
        fprintf(stderr, "no more JACK ports available\n");
        return;
    }

    process_args->input_port = input_port;
    process_args->output_port = output_port;

    /* Tell the JACK server that we are ready to roll.  Our
     * process() callback will start running now. */

    if (jack_activate(client)) {
        fprintf(stderr, "cannot activate client");
        return;
    }

    /* Connect the ports.  You can't do this before the client is
     * activated, because we can't make connections to clients
     * that aren't running.  Note the confusing (but necessary)
     * orientation of the driver backend ports: playback ports are
     * "input" to the backend, and capture ports are "output" from
     * it.
     */

    ports = jack_get_ports(client, nullptr, nullptr, JackPortIsPhysical | JackPortIsOutput);
    if (ports == nullptr) {
        fprintf(stderr, "no physical capture ports\n");
        return;
    }

    if (jack_connect(client, ports[0], jack_port_name(input_port))) {
        fprintf(stderr, "cannot connect input ports\n");
        return;
    }

    free(ports);

    ports = jack_get_ports(client, nullptr, nullptr, JackPortIsPhysical | JackPortIsInput);
    if (ports == nullptr) {
        fprintf(stderr, "no physical playback ports\n");
        sanitize();
        return;
    }

    if (jack_connect(client, jack_port_name(output_port), ports[0])) {
        fprintf(stderr, "cannot connect output1 port\n");
        sanitize();
        return;
    }

    if (jack_connect(client, jack_port_name(output_port), ports[1])) {
        fprintf(stderr, "cannot connect output2 port\n");
        sanitize();
        return;
    }

    free(ports);
}

void JackController::stop() {
    if (!is_running()) return;
    jack_client_close(client);
    sanitize();
}

void JackController::sanitize() {
    client = nullptr;
    input_port = nullptr;
    output_port = nullptr;

    delete process_args;
    process_args = nullptr;
}

bool JackController::is_running() {
    return client != nullptr;
}

void JackController::add_effect(EffectController *effect_controller) {
    m_effects.push_back(effect_controller);
}

void JackController::remove_effect(EffectController *effect_controller) {
    m_effects.erase(std::remove(m_effects.begin(), m_effects.end(), effect_controller));
}

void JackController::set_running(bool meant_to_run) {
    if (meant_to_run && !is_running()) {
        start();
        emit state_changed(true);
    } else if (!meant_to_run && is_running()) {
        stop();
        emit state_changed(false);
    }
}
