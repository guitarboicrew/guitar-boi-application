#include <jack/types.h>
#include "effect_controller.h"

#ifndef GUITARBOI_JACK_H
#define GUITARBOI_JACK_H

typedef std::vector<EffectController*> jack_effects;

typedef struct {
    jack_port_t *input_port;
    jack_port_t *output_port;
    jack_effects *used_effects;
} jack_process_args;

int jack_process(jack_nframes_t nframes, void *arg) ;

void jack_shutdown(void *arg);

#endif //GUITARBOI_JACK_H
