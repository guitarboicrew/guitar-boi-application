#include "jack.h"
#include <QObject>

#ifndef JACKCONTROLLER_H
#define JACKCONTROLLER_H

class JackController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(jack_effects effects MEMBER m_effects)

public:
    explicit JackController(QObject *parent = nullptr);
    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();
    Q_INVOKABLE bool is_running();

    Q_INVOKABLE void add_effect(EffectController *effect_controller);
    Q_INVOKABLE void remove_effect(EffectController *effect_controller);

    void sanitize();

signals:
    void state_changed(const bool running);

public slots:
    void set_running(bool meant_to_run);

private:
    jack_client_t *client = nullptr;
    jack_port_t *input_port = nullptr;
    jack_port_t *output_port = nullptr;
    jack_process_args *process_args = nullptr;

    jack_effects m_effects;
};

#endif // JACKCONTROLLER_H
