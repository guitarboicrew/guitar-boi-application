import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick 2.10
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Universal 2.1
import guitarboi.jackcontroller 1.0
import guitarboi.arcifycontroller 1.0

Window {
    visible: true
    width: 800
    height: 480
    title: qsTr("Guitar Boi")

    JackController {
        id: jackController
    }

    ArcifyController {
        id: arcifyController
    }

    Dial {
        id: arcifyDial
        from: 1
        to: 1000
        x: 463
        y: 148
        value: arcifyController.multiplier = value
    }

    Switch {
        id: arcifySwitch
        x: 498
        y: 382
        text: qsTr("Arcify")
        checked: arcifyController.enabled = checked
    }

    Switch {
        id: jackSwitch
        x: 112
        y: 220
        text: qsTr("JACK")
        checked: jackController.set_running(checked)
    }

    Component.onCompleted: {
        jackController.add_effect(arcifyController)
    }
}
