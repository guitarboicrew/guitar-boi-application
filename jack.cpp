#include <cstdio>
#include <cerrno>
#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <jack/jack.h>
#include <csignal>
#include "jack_controller.h"
#include "jack.h"

int jack_process(jack_nframes_t nframes, void *arg) {
    jack_default_audio_sample_t *in, *out;
    auto helper = reinterpret_cast<jack_process_args*>(arg);

    in = (jack_default_audio_sample_t *) jack_port_get_buffer(helper->input_port, nframes);
    out = (jack_default_audio_sample_t *) jack_port_get_buffer(helper->output_port, nframes);

    memcpy(out, in, sizeof(jack_default_audio_sample_t) * nframes);

    for (auto effect : *(helper->used_effects)) {
        effect->apply(out, nframes);
    }

    return 0;
}

/**
 * JACK calls this shutdown_callback if the server ever shuts down or
 * decides to disconnect the client.
 */
void jack_shutdown(void *arg) {
    auto jack_controller = reinterpret_cast<JackController*>(arg);
    jack_controller->sanitize();
}