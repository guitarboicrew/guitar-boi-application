#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "jack.h"
#include "arcify_controller.h"
#include "jack_controller.h"

void registerTypes() {
    qmlRegisterType<JackController>("guitarboi.jackcontroller", 1, 0, "JackController");
    qmlRegisterType<ArcifyController>("guitarboi.arcifycontroller", 1, 0, "ArcifyController");
}

int main(int argc, char *argv[]) {
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    registerTypes();

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
