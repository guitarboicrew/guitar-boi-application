#include "arcify_controller.h"
#include <cmath>

ArcifyController::ArcifyController(QObject *parent) : EffectController(parent)
{

}

void ArcifyController::apply_effect(float *out, jack_nframes_t len) {
    for (unsigned int i = 0; i < len; i++) {
        out[i] = (float) (atan(out[i] * m_multiplier) / atan(m_multiplier));
    }
}
