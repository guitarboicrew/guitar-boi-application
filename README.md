# Guitar Boi

This is repository of the application part of Guitar Boi - an amplifier and effect applier solution for Raspberry Pi 3 
equipped with official 7-inch touchscreen display and Rockstar USB cable.

## Getting started

### Preparing system

#### Installing required packages

The system must be equipped with basic development tools like make and C/C++ compiler. 
CMake is used as the project management solution.

In order to develop the solution wholly on the workstation, it is necessary to install JACK library and ALSA subsystem,
accompanied by the Qt5 with all the neccessary components (for example Qt5 QuickControls 2).

On Arch Linux one can obtain them by executing in shell as root:

`# pacman -S jack alsa-lib alsa-utils qt5-base qt5-quickcontrols2`

#### Running JACK daemon without root

By default only root user is able to start JACK daemon in realtime mode. 
In order to make it possible for your own user to do the same during development, add yourself to the audio group:

`$ sudo usermod -aG audio $USER`

Then, add the following line to _/etc/security/limits.conf_ file:

`@audio -   rtprio  99`

Log out and log in again in order to refresh information about user groups. 
You can now start JACK deamon and Guitar Boi application without root.

#### Starting JACK daemon

In order to start JACK daemon it is necessary to know the input and output devices' playback and capture ports
by calling respectively `arecord -l` and `aplay -l` commands. The JACK daemon command should look more or less like:

`$ jackd -dalsa -r48000 -p1024 -n2 -Chw:2,0,0 -Phw:2,0,0`

It might be necessary to adjust sample rate (`-r` argument) 
and number of frames (period) between JACK `process()` calls (`-p` argument).

### Preparing IDE

In order to simplify the development it is advised 
to use proper IDE like [CLion](https://www.jetbrains.com/clion/) or [Qt Creator](https://www.qt.io/download).

It is necessary to create appropriate development profiles in the IDE so that one of the profiles
points on the workstation's default toolchain and uses system versions of the libraries 
and the profile, which is dedicated for cross-compilation and cross-debugging on the Raspberry Pi and utilizes
Guitar Boi SDK's toolchain and sysroot.

What's more, a several CMake options have to be provided so that the Qt5 modules are resolved properly:

`-DQt5Core_DIR=<SDK_DIR>/arm-guitarboi-linux-uclibcgnueabihf/sysroot/usr/lib/cmake/Qt5Core`

`-DQt5Gui_DIR=<SDK_DIR>/arm-guitarboi-linux-uclibcgnueabihf/sysroot/usr/lib/cmake/Qt5Gui`

`-DQt5Network_DIR=<SDK_DIR>/arm-guitarboi-linux-uclibcgnueabihf/sysroot/usr/lib/cmake/Qt5Network`

`-DQt5Qml_DIR=<SDK_DIR>/arm-guitarboi-linux-uclibcgnueabihf/sysroot/usr/lib/cmake/Qt5Qml`

`-DQt5Quick_DIR=<SDK_DIR>/arm-guitarboi-linux-uclibcgnueabihf/sysroot/usr/lib/cmake/Qt5Quick`

`-DQt5_DIR=<SDK_DIR>/arm-guitarboi-linux-uclibcgnueabihf/sysroot/usr/lib/cmake/Qt5`

where `<SDK_DIR>` is the path to the unpacked Guitar Boi SDK.

## Authors

* **Konrad Ponichtera**
* **Giorgi Mamatsashvili**
