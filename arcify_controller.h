#ifndef ARCIFYCONTROLLER_H
#define ARCIFYCONTROLLER_H

#include "effect_controller.h"

#include <QObject>

class ArcifyController : public EffectController
{
    Q_OBJECT
    Q_PROPERTY(double multiplier MEMBER m_multiplier NOTIFY multiplier_changed)
public:
    explicit ArcifyController(QObject *parent = nullptr);

    void apply_effect(float *out, jack_nframes_t len) override;

signals:
    void multiplier_changed(const double new_multiplier);

public slots:


private:
    double m_multiplier = 1;

};

#endif // ARCIFYCONTROLLER_H
