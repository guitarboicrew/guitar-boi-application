#include <jack/types.h>

#include <QObject>

#ifndef EFFECT_CONTROLLER_H
#define EFFECT_CONTROLLER_H

class EffectController : public QObject {
    Q_OBJECT
    Q_PROPERTY(bool enabled MEMBER m_enabled NOTIFY enabled_changed)

public:

    void apply(float *out, jack_nframes_t len);

signals:
    void enabled_changed();

protected:
    bool m_enabled = false;

    virtual void apply_effect(float *out, jack_nframes_t len) = 0;

    explicit EffectController(QObject *parent = nullptr);

    virtual ~EffectController() {}

};

#endif // EFFECT_CONTROLLER_H
